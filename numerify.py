import pandas as pd

UNKNOWN = 0

if __name__ == '__main__':
    train_data = pd.read_csv("Train_BigMart.csv")
    test_data = pd.read_csv("Test_BigMart.csv")


    def categorize_col(df: pd.DataFrame, col: str) -> pd.DataFrame:
        df[col] = pd.Categorical(df[col])
        dummies = pd.get_dummies(df[col], prefix=col)
        df = pd.concat([df, dummies], axis=1)
        del df[col]
        return df


    def categorize_cols(df: pd.DataFrame, cols: [str]) -> pd.DataFrame:
        for col in cols:
            df = categorize_col(df, col)
        return df


    cols_cat = [
        "Item_Identifier",
        "Outlet_Identifier",
        "Outlet_Location_Type",
        "Outlet_Size",
        "Outlet_Type",
        "Item_Type",
        "Item_Fat_Content",
    ]
    train_data = categorize_cols(train_data, cols_cat)
    test_data = categorize_cols(test_data, cols_cat)

    # treat null weights
    train_data["Has_Weight"] = train_data.apply(axis=1, func=lambda row: 0 if pd.isna(row.Item_Weight) else 1)
    test_data["Has_Weight"] = train_data.apply(axis=1, func=lambda row: 0 if pd.isna(row.Item_Weight) else 1)

    train_data.fillna(0, inplace=True)
    test_data.fillna(0, inplace=True)

    train_data.to_csv("Train_BigMart_mod.csv", index=False)
    test_data.to_csv("Test_BigMart_mod.csv", index=False)
