from tensorflow import keras
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def plot_history(h):
    hist = pd.DataFrame(h.history)
    hist['epoch'] = h.epoch

    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('Mean Square Error')
    plt.plot(hist['epoch'], hist['mean_squared_error'],
             label='Train Error')
    # plt.plot(hist['epoch'], hist['val_mean_squared_error'],
    #          label='Val Error')
    plt.ylim([0, 0.02])
    plt.legend()
    plt.show()


if __name__ == '__main__':
    train_data = pd.read_csv("Train_BigMart_mod.csv")

    columns = train_data.columns.values
    np_train = train_data[columns[columns != "Item_Outlet_Sales"]].values
    np_labels = train_data[["Item_Outlet_Sales"]].values
    # label_max = np_labels.max()
    # np_labels = np_labels / label_max

    model = keras.models.Sequential([
        keras.layers.Dense(2048, activation=keras.activations.relu, input_shape=[len(columns) - 1]),
        keras.layers.Dense(2048, activation=keras.activations.relu),
        keras.layers.Dense(1),
    ])
    model.compile(
        optimizer=keras.optimizers.RMSprop(),
        loss=keras.losses.mean_squared_error,
        metrics=["mean_absolute_error", "mean_squared_error"],
    )
    model.summary()

    history = model.fit(x=np_train, y=np_labels, epochs=1000, batch_size=256,
                        callbacks=[keras.callbacks.EarlyStopping(monitor='mean_squared_error', patience=50)])
    plot_history(history)

    model.save("./model.hd5")
