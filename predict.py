import pandas
from tensorflow import keras

if __name__ == '__main__':
    model: keras.Model = keras.models.load_model("./model.hd5")

    test_data = pandas.read_csv("Test_BigMart_mod.csv")
    all_cols = pandas.read_csv("Train_BigMart_mod.csv").columns.values
    all_cols_df = pandas.DataFrame(columns=all_cols)
    test_data: pandas.DataFrame = pandas.concat([test_data, all_cols_df])
    test_data.fillna(0, inplace=True)
    del test_data["Item_Outlet_Sales"]

    np_test = test_data.values

    np_labels = model.predict(x=np_test)

    orig_test_data = pandas.read_csv("Test_BigMart.csv")
    orig_test_data["Item_Outlet_Sales"] = np_labels
    orig_test_data.to_csv("./Test_BigMart_Predicted.csv", index=False)
