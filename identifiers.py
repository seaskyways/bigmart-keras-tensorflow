import pandas as pd


def identify_column(df: pd.DataFrame, out, column):
    df = df.dropna(subset=[column])
    counter = 0
    identifiers = {}
    for (_, row) in df.iterrows():
        identifier = row[column]
        if identifier not in identifiers:
            counter = counter + 1
            identifiers[identifier] = counter
    df = pd.DataFrame.from_dict(
        data={"key": [key for key in identifiers.keys()], "value": [key for key in identifiers.values()]})
    df.to_csv(out, index=False)


if __name__ == '__main__':
    csv = pd.read_csv("./Train_BigMart.csv")
    identify_column(csv, "./ids/Item_Identifiers.csv", "Item_Identifier")
    identify_column(csv, "./ids/Item_Types.csv", "Item_Type")
    identify_column(csv, "./ids/Item_Outlet_Identifiers.csv", "Outlet_Identifier")
    identify_column(csv, "./ids/Item_Outlet_Sizes.csv", "Outlet_Size")
    identify_column(csv, "./ids/Item_Outlet_Location_Types.csv", "Outlet_Location_Type")
    identify_column(csv, "./ids/Item_Outlet_Types.csv", "Outlet_Type")
